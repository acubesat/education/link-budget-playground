import NumberParameter from "./parameters/NumberParameter";
import SelectParameter from "./parameters/SelectParameter";
import ToggleParameter from "./parameters/ToggleParameter";

export default {
  // Space segment parameters
  space: {
    txPower: new NumberParameter("Transmission power", 1, 0, 10, 0.1, "W", "The transmission power of the satellite"),
    frequency: new NumberParameter("Frequency", 2400, 100, 20000, 100, "MHz", "The central transmission frequency of the satellite"),
    dataRate: new NumberParameter("Data rate", 10, 1, 100000, 1, "kbps", "The speed of data transmission"),
    errorCorrection: new SelectParameter("Error correction algorithm", "0", {
      "0": "None",
      "1/2": "CCSDS LDPC 1/2",
      "2/3": "CCSDS LDPC 2/3",
      "3/4": "CCSDS LDPC 3/4",
      "4/5": "CCSDS LDPC 4/5"
    }, "The algorithm used to provide bit error correction for received messages. \
    Lower rate means lower data transmission speed, but higher resilience to errors"),
    polarity: new ToggleParameter("Antenna polarisation", false, "Linear", "Circular",
      "Transmitting the signal in a linearly polarised manner may result in losses due to Faraday rotation.")
  },
  ground: {}
}
