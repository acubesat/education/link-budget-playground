import Parameter from "./Parameter";

export default function SelectParameter(name, defaultValue = null, possibleOptions = {}, description = null) {
  Parameter.call(this, name, defaultValue, description);

  this.defaultValue = defaultValue;
  this.possibleOptions = possibleOptions;

  this.component = 'SelectParameterView';
}
