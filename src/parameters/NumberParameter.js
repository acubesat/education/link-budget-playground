import Parameter from "./Parameter";

export default function NumberParameter(name, defaultValue = 0, min = undefined, max = undefined, step = 1, units = null, description = null) {
  Parameter.call(this, name, defaultValue, description);

  this.defaultValue = defaultValue;
  this.min = min;
  this.max = max;
  this.step = step;
  this.units = units;
  this.description = description;

  this.component = 'NumberParameterView';
}
