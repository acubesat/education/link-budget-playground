export default function Parameter(name, defaultValue = null, description = null) {
  // The display name of the parameter
  this.name = name;
  // The default value of the object if no value is given by the user
  this.defaultValue = defaultValue;
  // The current value of the parameter
  this.value = defaultValue;
  // A description shown to the user if not null
  this.description = description;
  // A function returing a list of additions to the cost budget
  this.priceCalculator = function() {
    return [];
  }
  // A function returning any validation errors
  this.validator = function() {
    return [];
  }
}

// Get the value of this parameter, in the SI system of units if applicble
Parameter.prototype.getCanonicalValue = function() {
  return this.value;
}

// Alias for Parameter.getCanonicalValue
Parameter.prototype.cv = function() {
  return this.getCanonicalValue();
}
