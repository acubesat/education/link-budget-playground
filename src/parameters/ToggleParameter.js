import Parameter from "./Parameter";

export default function ToggleParameter(name, defaultValue = false, falseText = null, trueText = null, description = null) {
  Parameter.call(this, name, defaultValue, description);

  this.falseText = falseText;
  this.trueText = trueText;

  this.component = 'ToggleParameterView';
}
