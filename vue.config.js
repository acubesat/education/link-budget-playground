module.exports = {
  publicPath: "/link-budget-playground/"
};

const {gitDescribe, gitDescribeSync} = require('git-describe');
process.env.VUE_APP_GIT_VERSION = gitDescribeSync().semverString || gitDescribeSync().hash